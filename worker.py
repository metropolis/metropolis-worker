#!/usr/bin/python


import os

import sys
import json

from subprocess import Popen, PIPE
import pika
import threading



class RMQWorker(object):
	def __init__(self):
		
		abspath = os.path.abspath(__file__)
		dname = os.path.dirname(abspath)
		os.chdir(dname)

		host = open("scheduler_ip").read()
		credentials = pika.PlainCredentials('test', 'test')
		parameters = pika.ConnectionParameters(host=host,credentials=credentials)

		self.connection = pika.BlockingConnection(parameters)
		self.channel = self.connection.channel()
		self.channel.queue_declare(queue='task_queue', durable=True)
		
		self.channel.queue_declare(queue='result_queue', durable=True)

		self.channel.basic_qos(prefetch_count=1)
		self.channel.basic_consume(self.callback,
        	              queue='task_queue')

		threading.Thread(target=self.start_consuming).start()


	def start_consuming(self):
		self.channel.start_consuming()


	def callback(self,ch, method, properties, body):
		os.chdir("bin/Debug")
		message = json.loads(body)
		zone = message["zone"]
		probabilities = message["probabilities"]

		cmd = "./metropolis %s %s" % (zone, " ".join(str(x) for x in probabilities))
		try:
			p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
			out,err = p.communicate()
			print err,out
		except :
			e = sys.exc_info()[0]
			#print e
		finally:
			os.chdir("../../")
		
		ch.basic_ack(delivery_tag = method.delivery_tag)
		self.channel.basic_publish(exchange='',
	                      routing_key='result_queue',
	                      body=out,
	                      properties=pika.BasicProperties(
	                         delivery_mode = 2, # make message persistent
	                      ))

	def __del__(self):
		self.connection.close()

if __name__ == "__main__":
	rsc = RMQWorker()
	print "print"
