#include "task.h"
#include <functional>


class Custom : public Task
{
public:
    //Rosenbrock(size_t n);
    Custom(size_t n, Zone zone) : Task(n, zone){}

    Custom(std::vector<std::vector<double> > &X, std::vector<double> &y, Zone zone) : Task(X, y, zone){}


    double compute()
    {
        return compute(State);
    }

    double sqr(double a)
    {
        return a*a;
    }

    double compute(std::vector<double> &V)
    {
        double x = V.at(0);
        double y = V.at(1);
        return sqr(x) + sqr(y);
    }
};


