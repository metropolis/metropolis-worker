#include "task.h"
#include <functional>

#include<iostream>

Task::Task(size_t n, Zone zone)
{
    this->n_arguments = n;
    this->State.resize(n);
    this->zone = zone;
}

Task::Task(std::vector<std::vector<double> > &X, std::vector<double> &y,Zone zone)
{
    this->X = X;
    this->y = y;
    this->n_arguments = X.at(0).size();
    this->State.resize(n_arguments);
    this->zone = zone;
}


void Task::set_parameter(size_t param, double value)
{
    this->State.at(param) = value;
}

std::vector<double> Task::get_state() const
{
    return this->State;
}

size_t Task::get_n_arguments() const
{
    return this->n_arguments;
}

double Task::get_parameter(size_t index) const
{
    return this->State.at(index);
}
