
#include <vector>
#include <fstream>
#include <cmath>
#include <deque>
#include <functional>
#include <iostream>
#include <fstream>
#include <utility>
#include "metropolis.h"

#include "random.hpp"
#include <assert.h>


#include "json.h"
cpt::Random rng;


int Metropolis::sign(double val) const{
    return (double(0) < val) - (val < double(0));
}


double Metropolis::f_rand(double fMin, double fMax) const
{
    return (fMax - fMin) * ( (double)rng.rand() ) + fMin;
}

int Metropolis::rand(int n) const
{
    return (int)(rng.rand()*1000) % n;
}

double Metropolis::sqr(double a) const
{
    return a*a;
}

bool cmpd(double a, double b, double eps = 1e-6)
{
    return fabs(a-b) < eps;
}

void Metropolis::build_zone(std::deque<std::pair<double, double> > &V)
{
    std::vector<double> start,end;
    for(auto i=V.begin(); i<V.end(); i++)
    {
        start.push_back((*i).first);
        end.push_back((*i).second);
    }

    Zone curr;
    curr.start = start;
    curr.end = end;
    curr.probability = 0.9;
    curr.hits = 0;
    Zones.push_back(curr);
}

void Metropolis::dfs(std::vector<std::vector<std::pair<double, double> > > &V,
         std::deque<std::pair<double, double> > &curr, int depth)
{
    if((size_t)depth == task->get_n_arguments())
    {
        build_zone(curr);
    }
    else
    {

        for(size_t i=0; i<V.at(depth).size(); i++)
        {
            curr.push_back(V.at(depth).at(i));
            dfs(V,curr,depth+1);
            curr.pop_back();

        }
    }
}

void Metropolis::build_zones()
{

    int n  = Zones.size();

    int d = task->get_n_arguments();

    double ai = pow(n,1.0/d);

    assert(cmpd(ai,(int)ai));
    std::vector<std::vector<std::pair<double, double> > > V;
    for(int axis = 0; axis<d; axis++)
    {


        double l = task->get_zone().start.at(axis);
        double r = task->get_zone().end.at(axis);
        double w = fabs(l-r)/ai;

        std::vector<std::pair<double, double> > tmp;
        while(l+w < r || cmpd(l+w,r))
        {

            tmp.push_back(std::make_pair(l,l+w));
            l += w;
        }
        V.push_back(tmp);
    }

    std::deque<std::pair<double, double> > tmp;
    Zones.clear();

    dfs(V, tmp, 0);

}

Metropolis::Metropolis(std::shared_ptr<Task> task, std::string config_fname)
{

    std::ifstream fin(config_fname);
    Json::Value root;
    fin>>root;


    this->task = task;
    this->ha = root.get("ha", "UTF-8" ).asDouble();
    this->m = root.get("m", "UTF-8" ).asDouble();
    this->S0 = root.get("S0", "UTF-8" ).asDouble();
    this->STD_THRESHOLD = root.get("STD_THRESHOLD", "UTF-8" ).asDouble();
    this->MIN_ITERS = root.get("MIN_ITERS", "UTF-8" ).asDouble();
    this->MAX_ITERS = root.get("MAX_ITERS", "UTF-8" ).asDouble();
    this->n_Deltas = root.get("n_Deltas", "UTF-8" ).asDouble();
    this->ZONE_TOLERANCE = root.get("ZONE_TOLERANCE", "UTF-8" ).asDouble();

    auto n_Zones = root.get("n_Zones", "UTF-8" ).asDouble();


    this->Zones.resize(n_Zones);
    this->working_zone = -1;

    build_zones();

}

void Metropolis::set_task(std::shared_ptr<Task> task)
{
    this->task = task;
}

int Metropolis::get_iters() const
{
    return iters;
}

double Metropolis::stdv() const
{
    double sum= 0;
    double sum_of_squares = 0;
    int  n = Deltas.size();
    for(int i=0;i<n;i++)
    {
        sum_of_squares += Deltas.at(i)*Deltas.at(i);
        sum += Deltas.at(i);
    }
    double q = sum_of_squares - (sum*sum)/(double)n;
    double std = sqrt(q/(double)(n-1));
    return std;
}

double Metropolis::shoot(double param, int param_index, double Ha) const
{
        double tmp = param + Ha;
        double paramp = INFINITY;
        Zone zone = task->get_zone();
        if(rng.rand() < ZONE_TOLERANCE)
            zone = Zones[working_zone];

        if(tmp < zone.start.at(param_index))
            paramp = 2 * zone.start.at(param_index) - param - Ha;
        else if(tmp > zone.start.at(param_index) && tmp < zone.end.at(param_index))
            paramp = tmp;
        else if(tmp > zone.end.at(param_index))
            paramp = 2 * zone.end.at(param_index) - param - Ha;

        assert(paramp != INFINITY);
        assert(zone.end.at(param_index) <= 1);
        if(paramp > 1)
            std::cout<<paramp<<' '<<tmp<<' '<<zone.start.at(param_index)<<' '<<zone.end.at(param_index) <<'\n';
        assert(paramp <= 1);

        return paramp;

}

void Metropolis::randomize_state()
{
    for(size_t i = 0; i<task->get_n_arguments(); i++)
        task->set_parameter(i, f_rand(Zones.at(working_zone).start.at(i), Zones.at(working_zone).end.at(i)));
}

void Metropolis::choose_zone()
{
    bool done = false;
    while(!done)
    {
        int zone_index = rand(Zones.size());
        double p = rng.rand();
        if(p < Zones.at(zone_index).probability)
        {
            working_zone = zone_index;
            //Zones[zone_index].probability *= 0.9;
            done = true;
        }
    }
}

bool is_inside(std::vector<double> &V, Zone zone)
{
    bool result = true;
    for(size_t i=0; i<V.size();i++)
    {
        if((V.at(i) < zone.start.at(i) || V.at(i) > zone.end.at(i)))
            result = false;
    }
    return result;
}

int Metropolis::find_zone(std::vector<double> &V) const
{
    int zone_index = -1;
    for(size_t i = 0; i<Zones.size(); i++)
    {
        if(is_inside(V, Zones.at(i)))
            zone_index = i;
    }
    assert(zone_index > -1);
    return zone_index;

}

void Metropolis::normalize_zone_probability(double l, double r)
{
    double maxx = -1, minn= 2;
    for(size_t i = 0; i < Zones.size();i++)
    {
        if(Zones.at(i).probability > maxx)
            maxx = Zones.at(i).probability;
        if(Zones.at(i).probability < minn)
            minn = Zones.at(i).probability;
    }

    double range = maxx - minn;
    if(range != 0)
        for(size_t i=0; i< Zones.size(); i++)
        {
            Zones.at(i).probability = ((Zones.at(i).probability - minn) / range) * (r-l) + l;
        }

}

void Metropolis::compute_zone_fitness(int hits)
{
    for(size_t i = 0; i<Zones.size(); i++)
    {
        if(Zones.at(i).hits > 0)
        {
            Zones.at(i).probability =  (double)Zones.at(i).hits / hits ;
            Zones.at(i).hits = 0;
        }
    }

    normalize_zone_probability(0.1,0.9);

}

void Metropolis::strike_zone(int zone_index)
{
    if(zone_index == -1)
    {
        choose_zone();
    } else {
        working_zone = zone_index;
    }

    double W, Ha;

    int hits = 0;
    int iters = 0;
    double curr_std = STD_THRESHOLD + 1;

    randomize_state();
    while(curr_std >= STD_THRESHOLD && iters++ < MAX_ITERS)
    {
        W = rng.rand();

        int param_index = rand(task->get_n_arguments());
        double param = task->get_parameter(param_index);

        std::vector<double> state = task->get_state();
        int zone_index = find_zone(state);
        Zones[zone_index].hits++;
        hits++;

        Ha = ha * (W - 0.5);

        //Assign new values
        double paramp = shoot(param, param_index, Ha);

        std::vector<double> args = task->get_state();
        std::vector<double> argsp = task->get_state();
        argsp.at(param_index) = paramp;

        double current_value = task->compute(args);
        double new_value = task->compute(argsp);


        std::vector<double> row = args;
        row.push_back(current_value);
        History.push_back(row);

        double delta = sqr(new_value) - sqr(current_value);
        double tmp = -delta / ((2*(m+1)* sqr(S0)));

        double p = std::min(1.0, (double)exp(tmp));

        if(p > rng.rand())
            task->set_parameter(param_index,paramp);

        Deltas.push_back(delta);
        Deltas_log.push_back(delta);
        if(Deltas.size() >= n_Deltas)
        {
            curr_std = stdv();
            Deltas.clear();
        }
    }


}

void Metropolis::run()
{
    rng.set_park_miller();
    rng.set_seed(3050132083219644153UL);
    std::cout<<rng.get_seed()<<'\n';

    choose_zone();

    // Randomize parameters
    randomize_state();

    double W, Ha;

    int iters = 0;
    bool done = false;
    int hits = 0;

    for(;!done && iters<MAX_ITERS;iters++)
    {

        W = rng.rand();

        int param_index = rand(task->get_n_arguments());
        double param = task->get_parameter(param_index);

        std::vector<double> state = task->get_state();
        int zone_index = find_zone(state);
        Zones[zone_index].hits++;
        hits++;

        Ha = ha * (W - 0.5);

        //Assign new values
        double paramp = shoot(param, param_index, Ha);

        std::vector<double> args = task->get_state();
        std::vector<double> argsp = task->get_state();
        argsp.at(param_index) = paramp;

        double current_value = task->compute(args);
        double new_value = task->compute(argsp);


        std::vector<double> row = args;
        row.push_back(current_value);
        History.push_back(row);

        double delta = sqr(new_value) - sqr(current_value);
        double tmp = -delta / ((2*(m+1)* sqr(S0)));

        double p = std::min(1.0, (double)exp(tmp));

        if(p > rng.rand())
            task->set_parameter(param_index,paramp);
        //assert(paramp <= 1);
        Deltas.push_back(delta);
        Deltas_log.push_back(delta);
        if(Deltas.size() >= n_Deltas)
        {
            double curr_std = stdv();
            Stds_log.push_back(curr_std);
            Deltas.clear();

            if(curr_std < STD_THRESHOLD)
            {

                compute_zone_fitness(hits);

                hits = 0;
                choose_zone();
                randomize_state();

                for(size_t i=0; i<Zones.size();i++)
                {
                    std::cout<<Zones.at(i).probability<<' ';
                }
                std::cout<<iters<<'\n';
                std::cout.flush();

            }
        }
    }

    this->iters = iters;
}

int Metropolis::get_n_zones() const
{
    return Zones.size();
}
void Metropolis::set_zone_configuration(std::vector<double> &V)
{
    for(size_t i=0; i<Zones.size(); i++)
        Zones.at(i).probability = V.at(i);
}

void Metropolis::write_deltas(std::string fname)
{
    std::ofstream fout(fname);
    for(double item : this->Deltas_log)
        fout<<item<<'\n';
    fout.close();
}


void Metropolis::write_stds(std::string fname)
{
    std::ofstream fout(fname);
    for(double item : this->Stds_log)
        fout<<item<<'\n';
    fout.close();
}

void Metropolis::write_history(std::string fname)
{
    std::ofstream fout(fname);
    for(auto item : this->History)
        fout<<item.at(0)<<" "<<item.at(1)<<" "<<item.at(2)<<'\n';

    fout.close();
}

void Metropolis::write_zones(std::string fname)
{

}

Json::Value Metropolis::zones_to_json()
{
    Json::Value out;
    for(size_t i=0; i<Zones.size(); i++)
    {
        out[(int)i]["hits"] = Zones[i].hits;
        out[(int)i]["probability"] = Zones[i].probability;
        //out[(int)i]["status"] = Zones[i].status;
    }
    return out;
}
