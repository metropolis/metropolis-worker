#include "task.h"
#include "interpolation.h"
#include <functional>
#include <iostream>
#include <map>
#include <cmath>
#include <set>

class Interpolation : public Task
{

private:
    struct TableData2D
    {
        alglib_impl::ae_vector x;
        alglib_impl::ae_vector y;
        alglib_impl::ae_matrix data;
        alglib_impl::ae_state state;
        alglib_impl::spline2dinterpolant coefficients;
    };


    TableData2D InterpolationMatrix;

public:
    Interpolation(size_t n, Zone zone) : Task(n, zone){}

    Interpolation(std::vector<std::vector<double> > &X, std::vector<double> &y, Zone zone) : Task(X, y, zone){
        std::vector<double> vX;
        std::vector<double> vY;
        std::vector<std::vector<double> > XY;
        std::map<std::pair<double, double>, double> M;
        std::set<double> Xset, Yset;



        for(size_t i = 0; i< X.size(); i++)
        {
            //std::cout<<i<<'\n';
            double curr_x = X.at(i).at(0);
            if(Xset.find(curr_x) == Xset.end())
            {
                vX.push_back(curr_x);
                Xset.insert(curr_x);
            }

            double curr_y = X.at(i).at(1);
            if(Yset.find(curr_y) == Yset.end())
            {
                vY.push_back(curr_y);
                Yset.insert(curr_y);
            }

            std::pair<std::pair<double,double>, double> tmp_pair = std::make_pair(std::make_pair(X.at(i).at(0), X.at(i).at(1)), y.at(i));
            M.insert(tmp_pair);
        }


        XY.resize(vX.size());
        //std::cout<<vX.size()<<" "<<vY.size()<<'\n';
        //return;
        for(size_t i=0;i<vX.size();i++)
        {
            //std::cout<<i<<'\n';
            XY.at(i).resize(vY.size());
            for(size_t j=0; j<vY.size();j++)
                XY.at(i).at(j) = M.at(std::make_pair(vX.at(i), vY.at(j)));
        }

        /*
        std::cout.precision(11);
        for(auto &x : vX)
        {
            for(auto &y : vY)
            {
                auto tmp = std::make_pair(x,y);
                std::cout<<x<<" "<<y<<" "<<M.at(tmp)<<'\n';
            }
        }
        return;
        */

        alglib_impl::ae_int_t n = vX.size();
        alglib_impl::ae_int_t m = vY.size();
        InterpolationMatrix.state.endianness = 1;
        alglib_impl::ae_vector_init(&InterpolationMatrix.x, 0, alglib_impl::DT_REAL, &InterpolationMatrix.state);
        alglib_impl::ae_vector_init(&InterpolationMatrix.y, 0, alglib_impl::DT_REAL, &InterpolationMatrix.state);
        alglib_impl::ae_matrix_init(&InterpolationMatrix.data, 0, 0, alglib_impl::DT_REAL, &InterpolationMatrix.state);
        alglib_impl::_spline2dinterpolant_init(&InterpolationMatrix.coefficients, &InterpolationMatrix.state);

        alglib_impl::ae_vector_set_length(&InterpolationMatrix.x, n, &InterpolationMatrix.state);
        alglib_impl::ae_vector_set_length(&InterpolationMatrix.y, m, &InterpolationMatrix.state);
        alglib_impl::ae_matrix_set_length(&InterpolationMatrix.data, m, n, &InterpolationMatrix.state);


        for (int i = 0; i<n; i++)
            InterpolationMatrix.x.ptr.p_double[i] = vX.at(i);

        for (int i = 0; i<m; i++)
            InterpolationMatrix.y.ptr.p_double[i] = vY.at(i);


        for (int i = 0; i<m; i++)
        {
            for (int j = 0; j<n; j++)
                InterpolationMatrix.data.ptr.pp_double[i][j] = XY.at(j).at(i);
        }
        alglib_impl::spline2dbuildbicubic(&InterpolationMatrix.x, &InterpolationMatrix.y, &InterpolationMatrix.data, m, n, &InterpolationMatrix.coefficients, &InterpolationMatrix.state);

        //double result = alglib_impl::spline2dcalc(&InterpolationMatrix.coefficients, 0.00000100, 0.00500100, &InterpolationMatrix.state);
        //std::cout<<result<<'\n'<<vY[1]<<'\n'    ;

    }

    void set_parameter(size_t param, double value)
    {
        this->State.at(param) = value;
    }

    std::vector<double> get_state() const
    {
        return this->State;
    }

    double compute()
    {
        return compute(State);
    }

    double sqr(double a)
    {
        return a*a;
    }

    bool cmpd(double a, double b, double eps = 1e-6)
    {
        return fabs(a-b) < eps;
    }


    double compute(std::vector<double> &V)
    {
        double x = V.at(0);
        double y = V.at(1);

        return alglib_impl::spline2dcalc(&InterpolationMatrix.coefficients, x, y, &InterpolationMatrix.state);
    }
};


/*
*/
