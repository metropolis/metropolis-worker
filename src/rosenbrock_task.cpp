#include "task.h"
#include <functional>


class Rosenbrock : public Task
{
public:
    //Rosenbrock(size_t n);
    Rosenbrock(size_t n, Zone zone) : Task(n, zone){}

    Rosenbrock(std::vector<std::vector<double> > &X, std::vector<double> &y, Zone zone) : Task(X, y, zone){}


    double compute()
    {
        return compute(State);
    }

    double sqr(double a)
    {
        return a*a;
    }

    double compute(std::vector<double> &V)
    {
        double x = V.at(0);
        double y = V.at(1);
        return sqr(1-x) + 100*sqr(y-sqr(x));
    }
};


