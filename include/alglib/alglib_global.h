#ifndef ALGLIB_GLOBAL_H
#define ALGLIB_GLOBAL_H


#if defined(_MSC_VER)
    //  Microsoft
    #define ALGLIB_EXPORT __declspec(dllexport)
    #define ALGLIB_EXPORT __declspec(dllimport)
#elif defined(_GCC)
    //  GCC
    #define ALGLIB_EXPORT __attribute__((visibility("default")))
    #define ALGLIB_EXPORT
#else
    //  do nothing and hope for the best?
    #define ALGLIB_EXPORT
    #define ALGLIB_EXPORT
#endif

#endif
