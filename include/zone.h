#ifndef ZONE_H
#define ZONE_H

#include<vector>


enum  Status { start, transit };

struct Zone {
    std::vector<double> start, end;
    double probability;
    int hits;
    Status status;
};

#endif // ZONE_H
