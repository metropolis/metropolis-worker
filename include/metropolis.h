#ifndef METROPOLIS_H_
#define METROPOLIS_H_

#include "task.h"
#include <string>
#include <deque>
#include <memory>
#include "json/json.h"

class Metropolis{
private:
	double ha, ZONE_TOLERANCE,
		   m, S0, STD_THRESHOLD, current_std, MIN_ITERS, MAX_ITERS, n_Deltas;
	int iters;
    int working_zone;


	std::deque<double> Deltas;
	std::shared_ptr<Task> task;

	std::vector<double> Deltas_log;
	std::vector<double> Stds_log;
	std::vector<std::vector<double> > History;
	std::vector<Zone> Zones;

	double x,y;


    /**
        Computes standard deviation based on Deltas

        @return Standard deviation
    */
	double stdv() const;

	/**
        Computes the sign of a number

        @return -1 if less than zero, 1 if bigger than zero, 0 if zero
    */
	int sign(double val) const;

	/**
        Generates random number in range

        @param fMin left boundary
        @param fMax right boundary
        @return a random double in the range
	*/
	double f_rand(double fMin, double fMax) const;

	/**
        Computes the square of a number

        @param val The number to be squared
        @return val*val
	*/
	double sqr(double val) const;


	/**
        Computes the next potential point in Markov Chain

        @param param The current value
        @param param_index The parameter to work with(axis)
        @param Ha Step size

        @return New point position on param_index axis
	*/
    double shoot(double param, int param_index, double Ha) const;

    /**
        Throws Metropolis to a random point in space
        Randomizes state vector
    */
    void randomize_state();

    /**
        Gets Task's zone and splits it in n_Zones works in n Dimensions
    */
    void build_zones();

    /**
        Depth first search algorithm is used to build zones in all dimensions

        @param V A reference to a vector of vector of pair of doubles,
                 contains pairs of boundaries on each axis
        @param curr A reference to a deque of pair of doubles that holds current
                    zone's configuration
        @param depth The recursion depth, when it reach number of dimensions value
                     it means that we can build zone based on 'curr' param
    */
    void dfs(std::vector<std::vector<std::pair<double, double> > > &V,
         std::deque<std::pair<double, double> > &curr, int depth);

    /**
        Method that build a zone and pushes it to global zone list

        @param V A reference to a deque of pair of doubles, contains one zone configuration,
                 a pair on each axis, representing bounaries
    */
    void build_zone(std::deque<std::pair<double, double> > &V);

    /**
        Chooses a random zone based on their probability(fitness)
    */
    void choose_zone();

    /**
        Find zone where the point desribed by vector V resides

        @param V A reference to vector that describes point location
        @return Index of the zone
    */
    int find_zone(std::vector<double> &V) const;

    /**
        Computes the probability(fitness) of each zone

        @param hits Total number of hits
    */
    void compute_zone_fitness(int hits);

    /**
        Normalizes zones probability, adjusts them in a range

        @param l Left boundary
        @param r Right boundary
    */
    void normalize_zone_probability(double l, double r);


public:
    /**
        Constructor that creates a Metropolis object with known config file

        @param task A smart pointer to the 'Task' that must be computed
        @param config_fname Config file name
    */
	Metropolis(std::shared_ptr<Task> task, std::string config_fname);

    /**
        Task setter, sets the 'Task' to be computed
    */

	void set_task(std::shared_ptr<Task> task);

    /**
        Method that runs Metropolis algorithm, used for debugging
    */
	void run();

    /**
        Number of iterations getter
    */
	int get_iters() const;

    /**
        Generates a random integer between 0 and n, [0;n)

        @return Random integer
    */
    int rand(int n) const;

    /**
        Starts Metropolis algorithm from certain zone,
        the starting point is choosen randomly(inside the zone)

        @param zone_index, the zone to start Metropolis from, -1 for random zone
    */
    void strike_zone(int zone_index);

    /**
        Every zone has its own probability(fitness), this method assigns each
        zone probability to a certain vallue

        @param V, a refferene to vector that contains probabilities
    */
    void set_zone_configuration(std::vector<double> &V);

    /**
        Number of zones getter

        @return number of zones
    */
    int get_n_zones() const;

    /**
        Method that converts vector of zones into a JSON object

        @return Json object containing zones
    */
    Json::Value zones_to_json();

    /**
        Method that writes all deltas (errors) to a file

        @param fname File name
    */
	void write_deltas(std::string fname);

    /**
        Method that writes all computed standard deviations to a file

        @param fname File name
    */
	void write_stds(std::string fname);


    /**
        Method that writes all history, it contains points on all axis

        @param fname File name
    */
	void write_history(std::string fname);


    /**
        Method that writes zone history to a file

        @param fname File name
    */
	void write_zones(std::string fname);


};

#endif
