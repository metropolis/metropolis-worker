#ifndef TASK_H
#define TASK_H
#include <vector>
#include <functional>
#include <zone.h>
class Task
{
protected:
    std::vector<std::vector<double> > X;
    std::vector<double> y;
    std::vector<double> State;
    size_t n_arguments;
    Zone zone;

public:
    /**
        Constructor that creates a Task with pre-defined data

        @param X The argument matrix
        @param y The vector that contains the results of f(X)
        @param zone The zone where metropolis can shoot
    */
    Task(std::vector<std::vector<double> > &X, std::vector<double> &y, Zone zone);

    /**
        Constructor that creates an empty Task, but with certain number of arguments

        @param n_arguments Number of arguments
        @param zone The zone where metropolis can shoot
    */
    Task(size_t n_arguments,Zone zone);


    /**
        Method that gets a certain parameter

        @param param The parameter that is queried
        @return The value of chosen parameter
    */
    double get_parameter(size_t param) const;

    /**
        Method that sets an argument in current state

        @param param The argument, aka its index
        @param value The value that is assignet to the argument
    */
    void set_parameter(size_t param, double value);


    /**
        Getter for the state vector

        @return vector of doubles, containing current state
    */
    std::vector<double> get_state() const;

    /**
        Get number of arguments

        @return Number of arguments / axix
    */
    size_t get_n_arguments() const;

    /**
        Computes the value of the Task at current state

        @return A double calculated from the function and data
    */
    virtual double compute() = 0;

    /**
        Computes the value of the Task at given state

        @return A double calculated from the function and data
    */
    virtual double compute(std::vector<double> &V) = 0;

    /**
        Get task's zone

        @return Zone
    */
    Zone get_zone() const
    {
        return zone;
    }
};

#endif // TASK_H
