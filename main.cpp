#include "metropolis.h"
#include "src/interpolation_task.cpp"
#include "src/rosenbrock_task.cpp"
#include "src/custom_task.cpp"

#include <iostream>
#include <fstream>

#include "matrix.h"

#include "random.hpp"
#include <string>

#include <cassert>
#include "json/json.h"


int main(int argc,char *argv[]) {



    Matrix M(40000,3);
    M.load_from_file("input2.txt");
    std::vector<std::vector<double> > X = M.slice(1,-1,1,2).to_vector();
    std::vector<double> y = M.slice(1,-1,3,3).transpose().to_vector()[0];

    Zone zone;
    zone.start.resize(2);
    zone.start[0] = 0;
    zone.start[1] = 0;
    //zone.start[2] = a_MIN;


    zone.end.resize(2);
    zone.end[0] = 1;
    zone.end[1] = 1;
    //zone.end[2] = a_MAX;


    std::shared_ptr<Task> task(new Interpolation(X,y, zone));


    std::shared_ptr<Task> task2(new Rosenbrock(2,zone));

    std::shared_ptr<Task> task3(new Custom(2,zone));


    Metropolis met(task, "config.txt");

    if(argc > 1)
    {
        int zone_index = atoi(argv[1]);
        if(argc > 2)
        {
            int n_Zones = met.get_n_zones();
            assert(argc-2 == n_Zones);
            std::vector<double> V(n_Zones);
            for(int i=0;i<n_Zones;i++)
                V[i] = atof(argv[i+2]);
            met.set_zone_configuration(V);
        }

        met.strike_zone(zone_index);
        Json::FastWriter fastWriter;
        std::string s = fastWriter.write(met.zones_to_json());
        std::cout<<s;
    }
    else
    {
        met.run();

        met.write_stds("std.out");
        met.write_history("data.out");
        met.write_deltas("delta.out");
    }
    return 0;
}

