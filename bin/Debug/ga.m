function ans = ga()
	nGenerations = 10;
	nPopulation = 10;
	D = 2;
	mutation_chance = 0.09;

	%Generate population
	Population = rand(nPopulation, D) .* randi([-1000 1000],nPopulation,1);

	ans = Population;

	nMutations = 0
	out = []
	for i = 1:1000
		res = crossover(Population, mutation_chance);
		
		Population = res.('Population');
		mutations = res.('mutations');
		nMutations += mutations;
		out = [out; [Population(1,:) rosenbrock(Population(1,:))]];
		% Population;
	end
	nMutations
	out
	ans = out
end


function ans = crossover(Population, mutation_chance)
	new = Population;
	mutations = 0;
	for i = 1:20
		x = randi(size(new,1));
		y = randi(size(new,1));
		g = randi(size(new,2));

		tmp = new(x,g);
		new(x,g) = new(y,g);
		new(y,g) = tmp;

		if (rand() < mutation_chance)
			% new(x,g) = rand() * randi([-100 100])
			% rand() * randi([-100 100])
			new(x,g) = rand() * randi([-100 100]);
			mutations += 1;
		end

		if (rand() < mutation_chance)
			mutations += 1;
			new(y,g) = rand() * randi([-100 100]);
		end
		
	end

	% Population
	% new

	tmp = [Population ; new];
	tmp = [tmp rosenbrock(tmp)];
	ans = sortrows(tmp,size(tmp,2));
	ans = ans(1:size(Population,1),1:end-1);
	res = struct('Population', ans, 'mutations', mutations);
	
	ans = res;
end
