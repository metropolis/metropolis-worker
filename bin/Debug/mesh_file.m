function mesh_file (file)
	A = dlmread(file);
	x = A(:,1);
	y = A(:,2);
	z = A(:,3);

	xx = linspace(min(x),max(x),200);
	yy = linspace(min(y),max(y),200);
	
	[xxx, yyy] = meshgrid(xx,yy) ;
	zzz = griddata(x,y,z,xxx,yyy) ; 
	mesh(xxx,yyy,zzz);


end