function mesh_function (func)

	n = 50
	tx = ty = linspace (-2, 2, n)';
	[xx, yy] = meshgrid (tx, ty);
	arg = [xx(:) yy(:)];
	tz = reshape(func(arg), n, n);
	
	mesh (tx, ty, tz);
	xlabel('X');
	ylabel('Y');

end