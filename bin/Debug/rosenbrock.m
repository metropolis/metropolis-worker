function ans = rosenbrock(X)
	x = X(:,1);
	y = X(:,2);
	ans = (1-x).^2 + 100.*(y-x.^2).^2;
end 

