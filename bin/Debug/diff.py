import sys
import math

f = open(sys.argv[1],"r").readlines()
g = open(sys.argv[2],"r").readlines()


i = 1

def cmpd(a,b, eps = 0.000001):
  return math.fabs(a-b) < eps

def cmpt(a,b):
  r = True
  #print(tuple(b))
  for item in zip(a,b):
    r = r and cmpd(item[0],item[1])
  return r

r = True
for item in zip(f,g):
  a = tuple(map(float, item[0].strip().split(' ')))
  b = tuple(map(float, item[1].strip().split(' ')))
  #print(tuple(b))
  print("{} {} {} {}\n".format(i, a, b, cmpt(a, b)))
  r = r and cmpt(a,b)
  if not r:
   raise
  try:
    #input("")
    pass
  except SyntaxError:
    pass
  i += 1
