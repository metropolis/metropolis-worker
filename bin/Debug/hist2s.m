## usage: [xx,yy,nn] = hist2d (features,x)
##        where features is a 2xN matrix, and x is the number of bins.
##
## Produce 2D histogram counts or plots.

## Author: jwe . . . Modified for 2D by Dave Jansing

function [xx,yy,nn] = hist2s(features,x)

  if (nargin~=2)
    usage ("[xx,yy,nn] = hist2d(features,x) or hist2d(features,x)");
  endif

  [m,n]=size(features);
  if (m~=2)
        error("hist2d: features must be 2xN matrix");
  endif
  max_val=max(max(features(:,:)));
  min_val=min(min(features(:,:)));
  n = x;
  if (n <= 0)
     error("hist2d: number of bins must be positive");
  endif
  delta = (max_val - min_val) / n / 2;
  x = linspace (min_val-delta, max_val+delta, n);
  y = x;
  cutoff = x + delta;

  freq=zeros(n,n);
  for i=1:n-1
        for j=1:n-1
                freq(i,j)=sum(features(1,:)>=cutoff(i) & 
features(1,:)<cutoff(i+1) & features(2,:)>=cutoff(j) & 
features(2,:)<cutoff(j+1));
        endfor
  endfor

  if (nargout==3)
        nn = freq;
        xx = x;
        yy = y;
  else
        mesh(x,y,freq)
  endif

endfunction

