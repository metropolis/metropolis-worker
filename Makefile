SRC_DIR = src
INCLUDE_DIR = include
BIN_DIR = bin
OBJ_DIR = obj
LIB_DIR = lib
DEBUG_DIR = Debug
RELEASE_DIR = Release

MKDIR = mkdir -p
RM = rm
CP = cp

MODE := DEBUG

CXX=g++
CFLAGS=-g -c -Wall -Iinclude/ -Iinclude/alglib -Iinclude/json  -std=c++1y 
LDFLAGS=-static -Llib -lalglib -ljsoncpp


SOURCES = $(wildcard src/*.cpp)
SOURCES += main.cpp
#SOURCES += $(wildcard src/alglib/*.cpp)

OBJECTS=$(SOURCES:%.cpp=$(OBJ_DIR)/$($(MODE)_DIR)/%.o)

EXECUTABLE=metropolis


all:  obj_directory $(EXECUTABLE)

rebuild: clean all

$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(OBJECTS) $(LDFLAGS) -o $(BIN_DIR)/$($(MODE)_DIR)/$@
 

$(OBJECTS):
	$(CXX) $(CFLAGS) -o $@ $(@:$(OBJ_DIR)/$($(MODE)_DIR)/%.o=%.cpp)

include_main:
	$(CP) main.cpp $(SRC_DIR)/main.cpp

obj_directory:
	$(MKDIR) $(OBJ_DIR)/$(DEBUG_DIR)/$(SRC_DIR)

clean:
	find -name *.o | xargs $(RM)
	

my:
	@echo $(SOURCES)
	@echo $(OBJECTS)


         
